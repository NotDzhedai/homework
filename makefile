run:
	go run ./app/services/homework/*.go

test:
	go test ./...

docker:
	docker build .

