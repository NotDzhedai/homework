// Package auth provides authentication and authorization support.
package auth

import (
	"errors"
	"fmt"

	"github.com/golang-jwt/jwt/v4"
)

var ErrForbidden = errors.New("attempted action is not allowed")

// Auth is used to authenticate clients. It can generate a token for a
// set of user claims and recreate the claims by parsing the token.
type Auth struct {
	method    jwt.SigningMethod
	keyFunc   func(t *jwt.Token) (interface{}, error)
	jwtSecret string
}

// New creates an Auth to support authentication/authorization.
func New(jwtSecret string) (*Auth, error) {
	method := jwt.SigningMethodEdDSA
	if method == nil {
		return nil, errors.New("configuring algorithm EdDSA")
	}

	keyFunc := func(t *jwt.Token) (interface{}, error) {
		return []byte(jwtSecret), nil
	}

	// Create the token parser to use. The algorithm used to sign the JWT must be
	// validated to avoid a critical vulnerability:
	// https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/

	a := Auth{
		method:    method,
		keyFunc:   keyFunc,
		jwtSecret: jwtSecret,
	}

	return &a, nil
}

func (a *Auth) GenerateToken(claims Claims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	str, err := token.SignedString([]byte(a.jwtSecret))
	if err != nil {
		return "", fmt.Errorf("signing token: %w", err)
	}

	return str, nil
}

func (a *Auth) ValidateToken(tokenStr string) (Claims, error) {
	var claims Claims
	token, err := jwt.ParseWithClaims(tokenStr, &claims, a.keyFunc)
	if err != nil {
		return Claims{}, fmt.Errorf("parsing token: %w", err)
	}

	if !token.Valid {
		return Claims{}, errors.New("invalid token")
	}

	return claims, nil
}
