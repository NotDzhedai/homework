package post

import (
	"homework/business/core/post/db"
	"time"
)

type Post struct {
	Id          string    `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	UserId      string    `json:"user_id"`
	DateCreated time.Time `json:"date_created"`
}

type NewPost struct {
	Title string `json:"title"`
	Body  string `json:"body"`
}

type UpdatePost struct {
	Title *string `json:"title"`
	Body  *string `json:"body"`
}

func toPostSlice(dbPsts []db.Post) []Post {
	pmts := make([]Post, len(dbPsts))
	for i, dbPost := range dbPsts {
		pmts[i] = Post(dbPost)
	}
	return pmts
}
