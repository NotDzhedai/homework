package db

import "time"

type Post struct {
	Id          string
	Title       string
	Body        string
	UserId      string
	DateCreated time.Time
}
