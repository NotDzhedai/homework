package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
)

type Store struct {
	log *log.Logger
	db  *sql.DB
}

func NewStore(log *log.Logger, db *sql.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// create
func (s Store) Create(ctx context.Context, pst Post) error {
	const q = `
		insert into posts (id, title, body, user_id)
		values
		(
			?, ?, ?, ?
		)
	`

	if _, err := s.db.ExecContext(
		ctx,
		q,
		pst.Id,
		pst.Title,
		pst.Body,
		pst.UserId,
	); err != nil {
		return fmt.Errorf("inserting post: %w", err)
	}

	return nil
}

// querybyid
func (s Store) QueryByID(ctx context.Context, postID string) (Post, error) {
	const q = `
		select * from posts where id=?
	`

	var pst Post
	if err := s.db.QueryRowContext(ctx, q, postID).Scan(
		&pst.Id,
		&pst.Title,
		&pst.Body,
		&pst.UserId,
		&pst.DateCreated,
	); err != nil {
		return Post{}, fmt.Errorf("query post by id [%s]: %w", postID, err)
	}

	return pst, nil
}

// query
func (s Store) Query(ctx context.Context) ([]Post, error) {
	const q = `
		select * from posts
	`

	rows, err := s.db.QueryContext(ctx, q)
	if err != nil {
		return nil, err
	}

	var psts []Post
	for rows.Next() {
		var pst Post
		if err := rows.Scan(
			&pst.Id,
			&pst.Title,
			&pst.Body,
			&pst.UserId,
			&pst.DateCreated,
		); err != nil {
			return nil, fmt.Errorf("scan rows: %w", err)
		}

		psts = append(psts, pst)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return psts, nil
}

// query
func (s Store) QueryByUser(ctx context.Context, usrID string) ([]Post, error) {
	const q = `
		select * from posts where user_id=?
	`

	rows, err := s.db.QueryContext(ctx, q, usrID)
	if err != nil {
		return nil, err
	}

	var psts []Post
	for rows.Next() {
		var pst Post
		if err := rows.Scan(
			&pst.Id,
			&pst.Title,
			&pst.Body,
			&pst.UserId,
			&pst.DateCreated,
		); err != nil {
			return nil, fmt.Errorf("scan rows: %w", err)
		}

		psts = append(psts, pst)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return psts, nil
}

func (s Store) Delete(ctx context.Context, postID, usrID string) error {
	const q = `
		delete from posts where id=? and user_id=?
	`

	if _, err := s.db.ExecContext(ctx, q, postID, usrID); err != nil {
		return err
	}

	return nil
}

func (s Store) Update(ctx context.Context, pst Post) error {
	const q = `
		update posts
		set
			title=?,
			body=?
		where id=?
	`

	if _, err := s.db.ExecContext(ctx, q, pst.Title, pst.Body, pst.Id); err != nil {
		return err
	}

	return nil
}
