package post_test

import (
	"context"
	"homework/business/core/post"
	"homework/business/core/user"
	"homework/business/data/dbtests"
	"log"
	"os"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestPost(t *testing.T) {
	db, teardown := dbtests.NewUnit(t, "test_db.db")
	t.Cleanup(teardown)

	nlog := log.New(os.Stdout, " TEST POST CORE ", log.Ltime|log.Lshortfile)

	userCore := user.NewCore(nlog, db)
	postCore := post.NewCore(nlog, db)

	t.Log("Start post testing")
	{
		ctx := context.Background()
		now := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)

		// create user
		nu := user.NewUser{
			Name:     "Somebody",
			Email:    "somebody@gmail.com",
			Password: "qwerty",
		}

		usr, _ := userCore.Create(ctx, nu, now) // already tested

		np := post.NewPost{
			Title: "test title",
			Body:  "test body",
		}

		pst, err := postCore.Create(ctx, np, usr.Id, now)
		if err != nil {
			t.Fatalf("\tShould be able to create post : %s.", err)
		}
		t.Logf("\t +++ Should be able to create post")

		saved, err := postCore.QueryByID(ctx, pst.Id)
		if err != nil {
			t.Fatalf("\tShould be able to retrieve post by ID: %s.", err)
		}
		t.Logf("\t +++ Should be able to retrieve post by ID")

		if diff := cmp.Diff(pst, saved); err != nil {
			t.Fatalf("\tShould get back the same post. Diff:\n%s", diff)
		}
		t.Logf("\t +++ Should get back the same post")

		posts, err := postCore.QueryByUser(ctx, usr.Id)
		if err != nil {
			t.Fatalf("\tShould be able to retrieve posts by user ID: %s.", err)
		}
		t.Logf("\t +++ Should be able to retrieve posts by user ID")

		if len(posts) != 1 {
			t.Fatalf("\tShould be able to retrieve posts with len 1. GOT: %d", len(posts))
		}
		t.Logf("\t +++ Should be able to retrieve posts with len 1")

		allPosts, err := postCore.Query(ctx)
		if err != nil {
			t.Fatalf("\tShould be able to retrieve all posts: %s.", err)
		}
		t.Logf("\t +++ Should be able to retrieve all posts")

		if len(allPosts) != 4 { // with seed data
			t.Fatalf("\tShould be able to retrieve all posts with len 4. GOT: %d", len(posts))
		}
		t.Logf("\t +++ Should be able to retrieve all posts")

		up := post.UpdatePost{
			Title: dbtests.StringPointer("updated"),
		}

		updateErr := postCore.Update(ctx, pst.Id, usr.Id, up)
		if updateErr != nil {
			t.Fatalf("\tShould be able to update post: %s.", err)
		}
		t.Logf("\t +++ Should be able to update post")

		updated, _ := postCore.QueryByID(ctx, pst.Id)

		if updated.Title != "updated" {
			t.Fatalf("\tShould be able to get updated title, GOT: %s", updated.Title)
		}
		t.Logf("\t +++ Should be able to get updated title")

		if err := postCore.Delete(ctx, pst.Id, usr.Id); err != nil {
			t.Fatalf("\tShould be able to delete post : %s.", err)
		}
		t.Logf("\t +++ Should be able to delete post")

		if _, err := postCore.QueryByID(ctx, pst.Id); err == nil {
			t.Fatalf("\tShould NOT be able to retrieve address : %s.", err)
		}
		t.Logf("\t +++ Should NOT be able to retrieve address")
	}
}
