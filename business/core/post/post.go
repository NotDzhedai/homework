package post

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"homework/business/core/post/db"
	"homework/business/sys/validate"
	"log"
	"time"
)

var (
	ErrNotFound              = errors.New("not found")
	ErrInvalidID             = errors.New("ID is not in its proper form")
	ErrAuthenticationFailure = errors.New("authentication failed")
	ErrDublicateKey          = errors.New("already exists")
	ErrForbidden             = errors.New("forbidden")
)

type Core struct {
	store db.Store
}

func NewCore(log *log.Logger, sqlDb *sql.DB) Core {
	return Core{
		store: db.NewStore(log, sqlDb),
	}
}

func (c Core) Create(ctx context.Context, np NewPost, userID string, now time.Time) (Post, error) {
	if err := validate.Check(np); err != nil {
		return Post{}, fmt.Errorf("validating data: %w", err)
	}

	dbPost := db.Post{
		Id:          validate.GenerateID(),
		Title:       np.Title,
		Body:        np.Body,
		UserId:      userID,
		DateCreated: now,
	}

	if err := c.store.Create(ctx, dbPost); err != nil {
		return Post{}, fmt.Errorf("create post in core: %w", err)
	}

	return Post(dbPost), nil
}

func (c Core) QueryByID(ctx context.Context, postID string) (Post, error) {
	if err := validate.CheckID(postID); err != nil {
		return Post{}, ErrInvalidID
	}

	dbPost, err := c.store.QueryByID(ctx, postID)
	if err != nil {
		return Post{}, fmt.Errorf("query post in core by id: %w", err)
	}

	return Post(dbPost), nil
}

func (c Core) Query(ctx context.Context) ([]Post, error) {
	dbPosts, err := c.store.Query(ctx)
	if err != nil {
		return nil, fmt.Errorf("query posts in core: %w", err)
	}

	return toPostSlice(dbPosts), nil
}

func (c Core) QueryByUser(ctx context.Context, userID string) ([]Post, error) {
	dbPosts, err := c.store.QueryByUser(ctx, userID)
	if err != nil {
		return nil, fmt.Errorf("query posts in core by userID: %w", err)
	}

	return toPostSlice(dbPosts), nil
}

func (c Core) Delete(ctx context.Context, postId, userId string) error {
	if err := validate.CheckID(postId); err != nil {
		return ErrInvalidID
	}

	if err := c.store.Delete(ctx, postId, userId); err != nil {
		return fmt.Errorf("delete post: %w", err)
	}

	return nil
}

func (c Core) Update(ctx context.Context, postId, userId string, up UpdatePost) error {
	if err := validate.CheckID(postId); err != nil {
		return ErrInvalidID
	}

	if err := validate.Check(up); err != nil {
		return fmt.Errorf("validating data: %w", err)
	}

	dbPost, err := c.store.QueryByID(ctx, postId)
	if err != nil {
		return fmt.Errorf("updating user: %w", err)
	}

	if dbPost.UserId != userId {
		return ErrForbidden
	}

	if up.Body != nil {
		dbPost.Body = *up.Body
	}
	if up.Title != nil {
		dbPost.Title = *up.Title
	}

	if err := c.store.Update(ctx, dbPost); err != nil {
		return fmt.Errorf("updating user: %w", err)
	}

	return nil
}
