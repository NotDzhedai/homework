package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
)

type Store struct {
	log *log.Logger
	db  *sql.DB
}

func NewStore(log *log.Logger, db *sql.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

//create user

func (s Store) Create(ctx context.Context, usr User) error {
	const q = `
		insert into users (id, name, email, password_hash)
		values
		(
			@id, @name, @email, @password_hash
		)
	`

	if _, err := s.db.ExecContext(
		ctx,
		q,
		sql.Named("id", usr.Id),
		sql.Named("name", usr.Name),
		sql.Named("email", usr.Email),
		sql.Named("password_hash", usr.PasswordHash),
	); err != nil {
		return fmt.Errorf("inserting user: %w", err)
	}

	return nil
}

// непотрібно зараз
func (s Store) QueryByID(ctx context.Context, userID string) (User, error) {
	const q = `
		SELECT * FROM users WHERE id=@id
	`

	var usr User
	if err := s.db.QueryRowContext(ctx, q, sql.Named("id", userID)).Scan(
		&usr.Id,
		&usr.Name,
		&usr.Email,
		&usr.PasswordHash,
		&usr.DateCreated,
	); err != nil {
		return User{}, fmt.Errorf("query user [%s]: %w", userID, err)
	}

	return usr, nil
}

// потрібно для авторизації
func (s Store) QueryByEmail(ctx context.Context, email string) (User, error) {
	const q = `
		SELECT * FROM users WHERE email=@email
	`

	var usr User
	if err := s.db.QueryRowContext(ctx, q, sql.Named("email", email)).Scan(
		&usr.Id,
		&usr.Name,
		&usr.Email,
		&usr.PasswordHash,
		&usr.DateCreated,
	); err != nil {
		return User{}, fmt.Errorf("query user [%s]: %w", email, err)
	}

	return usr, nil
}
