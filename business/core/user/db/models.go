package db

import "time"

type User struct {
	Id           string
	Name         string
	Email        string
	PasswordHash string
	DateCreated  time.Time
}
