package user

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"homework/business/core/user/db"
	"homework/business/sys/auth"
	"homework/business/sys/validate"
	"log"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"
)

var (
	ErrNotFound              = errors.New("not found")
	ErrInvalidID             = errors.New("ID is not in its proper form")
	ErrAuthenticationFailure = errors.New("authentication failed")
	ErrDublicateKey          = errors.New("already exists")
)

type Core struct {
	store db.Store
}

func NewCore(log *log.Logger, sqlDb *sql.DB) Core {
	return Core{
		store: db.NewStore(log, sqlDb),
	}
}

func (c Core) Create(ctx context.Context, nu NewUser, now time.Time) (User, error) {

	if err := validate.Check(nu); err != nil {
		return User{}, fmt.Errorf("validating data: %w", err)
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(nu.Password), bcrypt.DefaultCost)
	if err != nil {
		return User{}, fmt.Errorf("generating password hash: %w", err)
	}

	dbUsr := db.User{
		Id:           validate.GenerateID(),
		Name:         nu.Name,
		Email:        nu.Email,
		PasswordHash: string(hash),
		DateCreated:  now,
	}

	if err := c.store.Create(ctx, dbUsr); err != nil {
		return User{}, fmt.Errorf("create user in core: %w", err)
	}

	return dbUserToUser(dbUsr), nil
}

func (c Core) QueryByID(ctx context.Context, userId string) (User, error) {
	if err := validate.CheckID(userId); err != nil {
		return User{}, ErrInvalidID
	}

	dbUsr, err := c.store.QueryByID(ctx, userId)
	if err != nil {
		return User{}, fmt.Errorf("query user in core by id: %w", err)
	}

	return dbUserToUser(dbUsr), nil
}

func (c Core) QueryByEmail(ctx context.Context, email string) (User, error) {
	dbUsr, err := c.store.QueryByEmail(ctx, email)
	if err != nil {
		return User{}, fmt.Errorf("query user in core by email: %w", err)
	}

	return dbUserToUser(dbUsr), nil
}

func (c Core) Login(ctx context.Context, now time.Time, email, password string) (auth.Claims, error) {
	dbUsr, err := c.store.QueryByEmail(ctx, email)
	if err != nil {
		return auth.Claims{}, ErrNotFound
	}

	if err := bcrypt.CompareHashAndPassword([]byte(dbUsr.PasswordHash), []byte(password)); err != nil {
		return auth.Claims{}, ErrAuthenticationFailure
	}

	claims := auth.Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "homework api",
			Subject:   dbUsr.Id,
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now().UTC()),
		},
	}

	return claims, err
}
