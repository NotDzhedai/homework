package user

import (
	"homework/business/core/user/db"
	"time"
)

type User struct {
	Id          string    `json:"id"`
	Name        string    `json:"name"`
	Email       string    `json:"email"`
	DateCreated time.Time `json:"date_created"`
}

type NewUser struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func dbUserToUser(dbUsr db.User) User {
	return User{
		Id:          dbUsr.Id,
		Name:        dbUsr.Name,
		Email:       dbUsr.Email,
		DateCreated: dbUsr.DateCreated,
	}
}
