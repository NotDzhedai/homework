package user_test

import (
	"context"
	"homework/business/core/user"
	"homework/business/data/dbtests"
	"log"
	"os"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
)

func TestUser(t *testing.T) {
	db, teardown := dbtests.NewUnit(t, "test_db.db")
	t.Cleanup(teardown)
	nlog := log.New(os.Stdout, " TEST USER CORE ", log.Ltime|log.Lshortfile)

	core := user.NewCore(nlog, db)

	t.Log("Start user testing")
	{
		ctx := context.Background()
		now := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)

		nu := user.NewUser{
			Name:     "Somebody",
			Email:    "somebody@gmail.com",
			Password: "qwerty",
		}

		usr, err := core.Create(ctx, nu, now)
		if err != nil {
			t.Fatalf("\tShould be able to create user : %s.", err)
		}
		t.Logf("\t +++ Should be able to create user")

		saved, err := core.QueryByID(ctx, usr.Id)
		if err != nil {
			t.Fatalf("\tShould be able to retrieve user by ID: %s.", err)
		}
		t.Logf("\t +++ Should be able to retrieve user by ID")

		if diff := cmp.Diff(usr, saved); err != nil {
			t.Fatalf("\tShould get back the same user. Diff:\n%s", diff)
		}
		t.Logf("\t +++ Should get back the same user")

		savedByEmail, err := core.QueryByEmail(ctx, usr.Email)
		if err != nil {
			t.Fatalf("\tShould be able to retrieve user by email: %s.", err)
		}
		t.Logf("\t +++ Should be able to retrieve user by email")

		if diff := cmp.Diff(usr, savedByEmail); err != nil {
			t.Fatalf("\tShould get back the same user. Diff:\n%s", diff)
		}
		t.Logf("\t +++ Should get back the same user")
	}
}
