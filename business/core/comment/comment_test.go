package comment_test

import (
	"context"
	"homework/business/core/comment"
	"homework/business/core/post"
	"homework/business/core/user"
	"homework/business/data/dbtests"
	"log"
	"os"
	"testing"
	"time"
)

func TestUser(t *testing.T) {
	db, teardown := dbtests.NewUnit(t, "test_db.db")
	t.Cleanup(teardown)

	nlog := log.New(os.Stdout, " TEST COMMENT CORE ", log.Ltime|log.Lshortfile)

	userCore := user.NewCore(nlog, db)
	postCore := post.NewCore(nlog, db)
	commentCore := comment.NewCore(nlog, db)

	t.Log("Start comment testing")
	{
		ctx := context.Background()
		now := time.Date(2018, time.October, 1, 0, 0, 0, 0, time.UTC)

		// create user
		nu := user.NewUser{
			Name:     "Somebody",
			Email:    "somebody@gmail.com",
			Password: "qwerty",
		}

		usr, _ := userCore.Create(ctx, nu, now) // already tested

		np := post.NewPost{
			Title: "test title",
			Body:  "test body",
		}

		pst, _ := postCore.Create(ctx, np, usr.Id, now) // already tested

		nc := comment.NewComment{
			Comment: "test comment",
			PostId:  pst.Id,
		}

		cmnt, err := commentCore.Create(ctx, nc, usr.Id, now)
		if err != nil {
			t.Fatalf("\tShould be able to create comment : %s.", err)
		}
		t.Logf("\t +++ Should be able to create comment")

		cmntsForPost, err := commentCore.QueryByPost(ctx, pst.Id)
		if err != nil {
			t.Fatalf("\tShould be able to retrieve comments by postID: %s.", err)
		}
		t.Logf("\t +++ Should be able to retrieve comments by postID")

		if len(cmntsForPost) != 1 {
			t.Fatalf("\tShould be able to retrieve comments with len 1. GOT: %d", len(cmntsForPost))
		}
		t.Logf("\t +++ Should be able to retrieve comments with len 1")

		uc := comment.UpdateComment{
			NewComment: "updated",
		}

		updateErr := commentCore.Update(ctx, uc, cmnt.Id, usr.Id)
		if updateErr != nil {
			t.Fatalf("\tShould be able to update comment: %s.", err)
		}
		t.Logf("\t +++ Should be able to update comment")

		if err := commentCore.Delete(ctx, cmnt.Id, usr.Id); err != nil {
			t.Fatalf("\tShould be able to delete comment : %s.", err)
		}
		t.Logf("\t +++ Should be able to delete comment")

		posts, _ := commentCore.QueryByPost(ctx, pst.Id)
		if len(posts) != 0 {
			t.Fatalf("\tShould be able to retrieve comments with len 0. GOT: %d", len(cmntsForPost))
		}
		t.Logf("\t +++ Should be able to retrieve comments with len 0")
	}
}
