package comment

import (
	"homework/business/core/comment/db"
	"time"
)

type Comment struct {
	Id          string    `json:"id"`
	Comment     string    `json:"comment"`
	UserId      string    `json:"user_id"`
	PostId      string    `json:"post_id"`
	DateCreated time.Time `json:"date_created"`
}

type NewComment struct {
	Comment string `json:"comment"`
	PostId  string `json:"post_id"`
}

type UpdateComment struct {
	NewComment string `json:"new_comment"`
}

func toCommentSlice(dbComments []db.Comment) []Comment {
	pmts := make([]Comment, len(dbComments))
	for i, dbComment := range dbComments {
		pmts[i] = Comment(dbComment)
	}
	return pmts
}
