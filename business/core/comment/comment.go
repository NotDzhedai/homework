package comment

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"homework/business/core/comment/db"
	"homework/business/sys/validate"
	"log"
	"time"
)

var (
	ErrNotFound              = errors.New("not found")
	ErrInvalidID             = errors.New("ID is not in its proper form")
	ErrAuthenticationFailure = errors.New("authentication failed")
	ErrDublicateKey          = errors.New("already exists")
	ErrForbidden             = errors.New("forbidden")
)

type Core struct {
	store db.Store
}

func NewCore(log *log.Logger, sqlDb *sql.DB) Core {
	return Core{
		store: db.NewStore(log, sqlDb),
	}
}

// create comment
func (c Core) Create(ctx context.Context, np NewComment, userId string, now time.Time) (Comment, error) {
	if err := validate.Check(np); err != nil {
		return Comment{}, fmt.Errorf("validating data: %w", err)
	}

	dbComment := db.Comment{
		Id:          validate.GenerateID(),
		Comment:     np.Comment,
		UserId:      userId,
		PostId:      np.PostId,
		DateCreated: now,
	}

	if err := c.store.Create(ctx, dbComment); err != nil {
		return Comment{}, fmt.Errorf("create comment in core: %w", err)
	}

	return Comment(dbComment), nil
}

// delete comment
func (c Core) Delete(ctx context.Context, commentID, userId string) error {
	if err := validate.CheckID(commentID); err != nil {
		return ErrInvalidID
	}

	if err := c.store.Delete(ctx, commentID, userId); err != nil {
		return fmt.Errorf("delete comment: %w", err)
	}

	return nil
}

// update comment
func (c Core) Update(ctx context.Context, newComment UpdateComment, commentID, userID string) error {
	if err := validate.CheckID(commentID); err != nil {
		return ErrInvalidID
	}

	dbComment, err := c.store.QueryByID(ctx, commentID)
	if err != nil {
		return fmt.Errorf("update comment: %w", err)
	}

	if dbComment.UserId != userID {
		return ErrForbidden
	}

	dbComment.Comment = newComment.NewComment

	if err := c.store.Update(ctx, dbComment); err != nil {
		return fmt.Errorf("update comment: %w", err)
	}

	return nil
}

// get all comments for post
func (c Core) QueryByPost(ctx context.Context, postID string) ([]Comment, error) {
	dbComments, err := c.store.QueryByPost(ctx, postID)
	if err != nil {
		return nil, fmt.Errorf("query comments in core by postID: %w", err)
	}

	return toCommentSlice(dbComments), nil
}
