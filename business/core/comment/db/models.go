package db

import "time"

type Comment struct {
	Id          string
	Comment     string
	UserId      string
	PostId      string
	DateCreated time.Time
}
