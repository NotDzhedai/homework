package db

import (
	"context"
	"database/sql"
	"fmt"
	"log"
)

type Store struct {
	log *log.Logger
	db  *sql.DB
}

func NewStore(log *log.Logger, db *sql.DB) Store {
	return Store{
		log: log,
		db:  db,
	}
}

// create comment
func (s Store) Create(ctx context.Context, comment Comment) error {
	const q = `
		insert into comments (id, comment, user_id, post_id)
		values
		(
			?, ?, ?, ?
		)
	`

	if _, err := s.db.ExecContext(
		ctx,
		q,
		comment.Id,
		comment.Comment,
		comment.UserId,
		comment.PostId,
	); err != nil {
		return fmt.Errorf("inserting post: %w", err)
	}

	return nil
}

// delete comment
func (s Store) Delete(ctx context.Context, commentID, usrID string) error {
	const q = `
		delete from comments where id=? and user_id=?
	`
	fmt.Println("--------")
	fmt.Println(commentID)
	fmt.Println(usrID)
	fmt.Println("--------")

	if _, err := s.db.ExecContext(ctx, q, commentID, usrID); err != nil {
		return err
	}

	return nil
}

// update comment
func (s Store) Update(ctx context.Context, comment Comment) error {
	const q = `
		update comments
		set
			comment=?
		where id=?
	`

	if _, err := s.db.ExecContext(ctx, q, comment.Comment, comment.Id); err != nil {
		return err
	}

	return nil
}

// get all comments for post
// query
func (s Store) QueryByPost(ctx context.Context, postID string) ([]Comment, error) {
	const q = `
		select * from comments where post_id=?
	`

	rows, err := s.db.QueryContext(ctx, q, postID)
	if err != nil {
		return nil, err
	}

	var cmnts []Comment
	for rows.Next() {
		var cmnt Comment
		if err := rows.Scan(
			&cmnt.Id,
			&cmnt.Comment,
			&cmnt.UserId,
			&cmnt.PostId,
			&cmnt.DateCreated,
		); err != nil {
			return nil, fmt.Errorf("scan rows: %w", err)
		}

		cmnts = append(cmnts, cmnt)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return cmnts, nil
}

// querybyid
func (s Store) QueryByID(ctx context.Context, commentID string) (Comment, error) {
	const q = `
		select * from comments where id=?
	`

	var comment Comment
	if err := s.db.QueryRowContext(ctx, q, commentID).Scan(
		&comment.Id,
		&comment.Comment,
		&comment.UserId,
		&comment.PostId,
		&comment.DateCreated,
	); err != nil {
		return Comment{}, fmt.Errorf("query post by id [%s]: %w", commentID, err)
	}

	return comment, nil
}
