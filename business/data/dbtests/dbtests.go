package dbtests

import (
	"context"
	"database/sql"
	dbUser "homework/business/core/user/db"
	"homework/business/data/dbschema"
	"homework/business/sys/auth"
	"log"
	"os"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v4"
	_ "github.com/mattn/go-sqlite3"
)

func NewUnit(t *testing.T, dbTestName string) (*sql.DB, func()) {
	t.Log("creating new db...")

	file, err := os.Create(dbTestName)
	if err != nil {
		t.Fatal(err)
	}
	file.Close()

	db, err := sql.Open("sqlite3", dbTestName)
	if err != nil {
		t.Fatal(err)
	}

	// ? migrations
	// migrate
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.Migrate(ctx, db); err != nil {
		t.Fatal("Migrate fail: ", err)
	}
	// seed
	ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.Seed(ctx, db); err != nil {
		t.Fatal("Seed fail: ", err)
	}

	teardown := func() {
		t.Log("stopping database support")
		db.Close()
		os.Remove(dbTestName)
	}

	return db, teardown
}

// Test owns state for running and shutting down tests.
type Test struct {
	DB       *sql.DB
	Auth     *auth.Auth
	Teardown func()
	Log      *log.Logger

	t *testing.T
}

func NewIntegration(t *testing.T, dbTestName string, log *log.Logger) *Test {
	db, teardown := NewUnit(t, "test_db.db")

	auth, err := auth.New("secret")
	if err != nil {
		t.Fatal(err)
	}

	test := Test{
		DB:       db,
		Auth:     auth,
		Teardown: teardown,
		t:        t,
		Log:      log,
	}

	return &test
}

// generate token for tests
func (test *Test) Token(email, pass string) string {
	test.t.Log("Generating token for test ...")

	store := dbUser.NewStore(test.Log, test.DB)
	dbUsr, err := store.QueryByEmail(context.Background(), email)
	if err != nil {
		return ""
	}

	claims := auth.Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "homework tests",
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now().UTC()),
			Subject:   dbUsr.Id,
		},
	}

	token, err := test.Auth.GenerateToken(claims)
	if err != nil {
		test.t.Fatal(err)
	}

	return token
}

// for update tests
func StringPointer(s string) *string {
	return &s
}
