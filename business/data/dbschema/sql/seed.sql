insert into users (id, name, email, password_hash)
values 
(
  "2f9a5930-5e9d-4846-8b4a-fad937d3d838",
  "test",
  "test@gmail.com",
  "$2a$10$1ggfMVZV6Js0ybvJufLRUOWHS5f6KneuP0XwwHpJ8L8ipdry9f2/a"
),
(
  "d4fc8ebc-762c-45db-85ea-c592baa8fcf4",
  "test 2",
  "test2@gmail.com",
  "$2a$10$1ggfMVZV6Js0ybvJufLRUOWHS5f6KneuP0XwwHpJ8L8ipdry9f2/a"
);


insert into posts (id, title, body, user_id)
values
(
  "9cdbd60a-100e-49da-969b-853c87f84953",
  "Test Title",
  "Body of test post",
  "2f9a5930-5e9d-4846-8b4a-fad937d3d838"
),
(
  "a4db5d55-c4f9-402b-8d17-e61a945b01c0",
  "Test Title 2",
  "Body of test 2 post",
  "2f9a5930-5e9d-4846-8b4a-fad937d3d838"
),
(
  "b20a1633-5385-487e-81b3-d78e3ce798de",
  "Test Title 3",
  "Body of test 3 post",
  "d4fc8ebc-762c-45db-85ea-c592baa8fcf4"
);

insert into comments (id, comment, user_id, post_id)
values
(
  "caf2b495-8945-4c27-8255-d7249eac28bc",
  "comment to post 1 by user 1",
  "2f9a5930-5e9d-4846-8b4a-fad937d3d838",
  "9cdbd60a-100e-49da-969b-853c87f84953"
),
(
  "8c350f78-ea54-4ff9-89fe-33828f3c04b3",
  "comment to post 2 by user 1",
  "2f9a5930-5e9d-4846-8b4a-fad937d3d838",
  "a4db5d55-c4f9-402b-8d17-e61a945b01c0"
),
(
  "1465c43d-3f0a-4244-a9a0-f43e3b93cda2",
  "comment to post 3 by user 1",
  "2f9a5930-5e9d-4846-8b4a-fad937d3d838",
  "b20a1633-5385-487e-81b3-d78e3ce798de"
),
(
  "1452c6b0-dfff-4c0e-8da5-b5879e2e0a14",
  "comment to post 2 by user 2",
  "d4fc8ebc-762c-45db-85ea-c592baa8fcf4",
  "a4db5d55-c4f9-402b-8d17-e61a945b01c0"
),
(
  "0f4fc9ce-a8ae-4fbe-bd93-6ae52bdfefd8",
  "comment to post 3 by user 2",
  "d4fc8ebc-762c-45db-85ea-c592baa8fcf4",
  "b20a1633-5385-487e-81b3-d78e3ce798de"
);