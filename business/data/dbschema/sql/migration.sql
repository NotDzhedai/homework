create table users (
  id text primary key,
  name text not null,
  email text not null unique,
  password_hash text not null,
  date_created datetime default CURRENT_TIMESTAMP
);

create table posts (
  id text primary key,
  title text not null unique,
  body text not null,
  user_id text,
  date_created datetime default CURRENT_TIMESTAMP,
  foreign key (user_id) references users (id) on delete cascade
);

create table comments (
  id text primary key,
  comment text not null,
  user_id text,
  post_id text,
  date_created datetime default CURRENT_TIMESTAMP,
  foreign key (user_id) references users (id) on delete cascade
  foreign key (post_id) references posts (id) on delete cascade
);