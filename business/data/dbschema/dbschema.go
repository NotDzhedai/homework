package dbschema

import (
	"context"
	"database/sql"
	_ "embed"
	"fmt"
	"time"
)

var (
	//go:embed sql/seed.sql
	seedDoc string

	//go:embed sql/migration.sql
	migrateDoc string
)

func Migrate(ctx context.Context, db *sql.DB) error {
	if err := StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.Exec(migrateDoc); err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return tx.Commit()
}

func Seed(ctx context.Context, db *sql.DB) error {
	if err := StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	if _, err := tx.Exec(seedDoc); err != nil {
		if err := tx.Rollback(); err != nil {
			return err
		}
		return err
	}

	return tx.Commit()
}

// Повинна бути не тут, а мабуть в якомусь пакеті database в business/sys, але мені лінь
func StatusCheck(ctx context.Context, db *sql.DB) error {
	// First check we can ping the database.
	var pindError error
	for attempts := 1; ; attempts++ {
		pindError = db.Ping()
		if pindError == nil {
			break
		}
		time.Sleep(time.Duration(attempts) * 100 * time.Millisecond)
		if ctx.Err() != nil {
			return ctx.Err()
		}
	}

	// Make sure we didn't timeout or be cancelled.
	if ctx.Err() != nil {
		return ctx.Err()
	}

	// Run a simple query to determine connectivity. Running this query forces a
	// round trip through the database.
	const q = `SELECT true`
	var tmp bool
	return db.QueryRowContext(ctx, q).Scan(&tmp)
}
