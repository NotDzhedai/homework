package handlers

import (
	"context"
	"errors"
	"fmt"
	"homework/business/core/post"
	"homework/business/sys/auth"
	v1 "homework/business/web/v1"
	"homework/foundation/web"
	"net/http"
)

type PostHandlers struct {
	Core post.Core
	Auth *auth.Auth
}

// create post -> 201
func (ph PostHandlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	var np post.NewPost
	if err := web.Decode(r, &np); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	pst, err := ph.Core.Create(ctx, np, claims.Subject, v.Now)
	if err != nil {
		return fmt.Errorf("user[%+v]: %w", &pst, err)
	}

	return web.Respond(ctx, w, pst, http.StatusCreated)
}

// delete post -> 204
func (hp PostHandlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	postID := web.Param(r, "id")

	if err := hp.Core.Delete(ctx, postID, claims.Subject); err != nil {
		switch {
		case errors.Is(err, post.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", postID, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

// query post by id -> 200
func (hp PostHandlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	postID := web.Param(r, "id")

	pst, err := hp.Core.QueryByID(ctx, postID)
	if err != nil {
		switch {
		case errors.Is(err, post.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", postID, err)
		}
	}

	return web.Respond(ctx, w, pst, http.StatusOK)
}

// query posts -> 200
func (hp PostHandlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	pst, err := hp.Core.Query(ctx)
	if err != nil {
		switch {
		case errors.Is(err, post.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("query posts: %w", err)
		}
	}

	return web.Respond(ctx, w, pst, http.StatusOK)
}

// query posts by user -> 200
func (hp PostHandlers) QueryByUser(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	psts, err := hp.Core.QueryByUser(ctx, claims.Subject)
	if err != nil {
		switch {
		case errors.Is(err, post.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("query posts for user[%s]: %w", claims.Subject, err)
		}
	}

	return web.Respond(ctx, w, psts, http.StatusOK)
}

func (hp PostHandlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	var up post.UpdatePost
	if err := web.Decode(r, &up); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	postID := web.Param(r, "id")
	if err := hp.Core.Update(ctx, postID, claims.Subject, up); err != nil {
		switch {
		case errors.Is(err, post.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, post.ErrForbidden):
			return v1.NewRequestError(err, http.StatusForbidden)
		default:
			return fmt.Errorf("postID[%s] post[%+v]: %w", postID, &up, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}
