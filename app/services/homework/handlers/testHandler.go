package handlers

import (
	"context"
	"homework/business/sys/auth"
	"homework/foundation/web"
	"log"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

// Handlers manages the set of check enpoints.
type TestHandlers struct {
	Log  *log.Logger
	Auth *auth.Auth
}

// Test handler is for development.
func (h TestHandlers) Test(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	status := struct {
		Status string
	}{
		Status: "Hello",
	}

	return web.Respond(ctx, w, status, http.StatusOK)
}

func (h TestHandlers) TestToken(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	c := auth.Claims{
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour)),
			IssuedAt:  jwt.NewNumericDate(time.Now().UTC()),
		},
	}

	token, err := h.Auth.GenerateToken(c)
	if err != nil {
		return web.Respond(ctx, w, "ERROR", http.StatusInternalServerError)
	}

	status := struct {
		Token string
	}{
		Token: token,
	}

	return web.Respond(ctx, w, status, http.StatusOK)
}
