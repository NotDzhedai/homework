package handlers

import (
	"context"
	"database/sql"
	"homework/business/core/comment"
	"homework/business/core/post"
	"homework/business/core/user"
	"homework/business/sys/auth"
	"homework/business/web/v1/mid"
	"homework/foundation/web"
	"log"
	"net/http"
	"os"
)

type Options struct {
	corsOrigin string
}

func WithCORS(origin string) func(opts *Options) {
	return func(opts *Options) {
		opts.corsOrigin = origin
	}
}

type APIMuxConfig struct {
	Shutdown chan os.Signal
	Log      *log.Logger
	DB       *sql.DB
	Auth     *auth.Auth
}

func ApiMux(cfg APIMuxConfig, options ...func(opts *Options)) *web.App {
	var opts Options
	for _, op := range options {
		op(&opts)
	}

	var app *web.App

	if opts.corsOrigin != "" {
		app = web.NewApp(
			cfg.Shutdown,
			mid.Logger(cfg.Log),
			mid.Errors(cfg.Log),
			mid.Cors(opts.corsOrigin),
			mid.Panics(),
		)

		h := func(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
			return nil
		}
		app.Handle(http.MethodOptions, "", "/*", h)
		// app.Handle(http.MethodOptions, "", "/*", h, mid.Cors(opts.corsOrigin))
	}

	if app == nil {
		app = web.NewApp(
			cfg.Shutdown,
			mid.Logger(cfg.Log),
			mid.Errors(cfg.Log),
			mid.Panics(),
		)
	}

	set_endpoints(app, cfg)

	return app
}

func set_endpoints(app *web.App, cfg APIMuxConfig) {
	const version = "v1"

	testGrp := TestHandlers{
		Log:  cfg.Log,
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodGet, version, "/test", testGrp.Test, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/test_token", testGrp.TestToken)

	userGrp := UserHandlers{
		Core: user.NewCore(cfg.Log, cfg.DB),
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodPost, version, "/user", userGrp.Create)
	app.Handle(http.MethodGet, version, "/login", userGrp.Login)

	postGrp := PostHandlers{
		Core: post.NewCore(cfg.Log, cfg.DB),
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodPost, version, "/post", postGrp.Create, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/posts", postGrp.Query, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/post/:id", postGrp.QueryByID, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/posts_user", postGrp.QueryByUser, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodPut, version, "/post/:id", postGrp.Update, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodDelete, version, "/post/:id", postGrp.Delete, mid.Authenticate(cfg.Auth))

	commentGrp := CommmentHandlers{
		Core: comment.NewCore(cfg.Log, cfg.DB),
		Auth: cfg.Auth,
	}

	app.Handle(http.MethodPost, version, "/comment", commentGrp.Create, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodGet, version, "/comments/:id", commentGrp.QueryByPost, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodPut, version, "/comment/:id", commentGrp.Update, mid.Authenticate(cfg.Auth))
	app.Handle(http.MethodDelete, version, "/comment/:id", commentGrp.Delete, mid.Authenticate(cfg.Auth))
}
