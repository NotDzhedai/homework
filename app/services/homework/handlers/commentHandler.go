package handlers

import (
	"context"
	"errors"
	"fmt"
	"homework/business/core/comment"
	"homework/business/sys/auth"
	v1 "homework/business/web/v1"
	"homework/foundation/web"
	"net/http"
)

type CommmentHandlers struct {
	Core comment.Core
	Auth *auth.Auth
}

func (ch CommmentHandlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	var np comment.NewComment
	if err := web.Decode(r, &np); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	comment, err := ch.Core.Create(ctx, np, claims.Subject, v.Now)
	if err != nil {
		return fmt.Errorf("user[%+v]: %w", &comment, err)
	}

	return web.Respond(ctx, w, comment, http.StatusCreated)
}

// delete post -> 204
func (ch CommmentHandlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	commentId := web.Param(r, "id")

	if err := ch.Core.Delete(ctx, commentId, claims.Subject); err != nil {
		switch {
		case errors.Is(err, comment.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("ID[%s]: %w", commentId, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (ch CommmentHandlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return errors.New("claims missing from context")
	}

	commentId := web.Param(r, "id")

	var newComment comment.UpdateComment
	if err := web.Decode(r, &newComment); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	if err := ch.Core.Update(ctx, newComment, commentId, claims.Subject); err != nil {
		switch {
		case errors.Is(err, comment.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		case errors.Is(err, comment.ErrForbidden):
			return v1.NewRequestError(err, http.StatusForbidden)
		default:
			return fmt.Errorf("commentID[%s]: %w", commentId, err)
		}
	}

	return web.Respond(ctx, w, nil, http.StatusOK)
}

func (ch CommmentHandlers) QueryByPost(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	postID := web.Param(r, "id")

	psts, err := ch.Core.QueryByPost(ctx, postID)
	if err != nil {
		switch {
		case errors.Is(err, comment.ErrInvalidID):
			return v1.NewRequestError(err, http.StatusBadRequest)
		default:
			return fmt.Errorf("query comments for post[%s]: %w", postID, err)
		}
	}

	return web.Respond(ctx, w, psts, http.StatusOK)
}
