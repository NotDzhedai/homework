package handlers

import (
	"context"
	"errors"
	"fmt"
	"homework/business/core/user"
	"homework/business/sys/auth"
	v1 "homework/business/web/v1"
	"homework/foundation/web"
	"net/http"
)

type UserHandlers struct {
	Core user.Core
	Auth *auth.Auth
}

// create user -> 201
func (uh UserHandlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nu user.NewUser
	if err := web.Decode(r, &nu); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	usr, err := uh.Core.Create(ctx, nu, v.Now)
	if err != nil {
		switch {
		case errors.Is(err, user.ErrDublicateKey):
			return v1.NewRequestError(err, http.StatusForbidden)
		default:
			return fmt.Errorf("user[%+v]: %w", &usr, err)
		}
	}

	return web.Respond(ctx, w, usr, http.StatusCreated)
}

// get token -> 200
func (uh UserHandlers) Login(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	email, pass, ok := r.BasicAuth()
	if !ok {
		err := errors.New("must provide email and password in Basic auth")
		return v1.NewRequestError(err, http.StatusUnauthorized)
	}

	claims, err := uh.Core.Login(ctx, v.Now, email, pass)
	if err != nil {
		switch {
		case errors.Is(err, user.ErrNotFound):
			return v1.NewRequestError(err, http.StatusNotFound)
		case errors.Is(err, user.ErrAuthenticationFailure):
			return v1.NewRequestError(err, http.StatusUnauthorized)
		default:
			return fmt.Errorf("authenticating: %w", err)
		}
	}

	var tkn struct {
		Token string `json:"token"`
	}
	tkn.Token, err = uh.Auth.GenerateToken(claims)
	if err != nil {
		return fmt.Errorf("generating token: %w", err)
	}

	return web.Respond(ctx, w, tkn, http.StatusOK)
}
