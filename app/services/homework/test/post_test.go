package test

import (
	"homework/app/services/homework/handlers"
	"homework/business/data/dbtests"
	"log"
	"net/http"
	"os"
	"testing"
)

type PostTests struct {
	app       http.Handler
	userToken string
}

func TestPost(t *testing.T) {
	nlog := log.New(os.Stdout, " TEST POST ", log.Ltime|log.Lshortfile)
	test := dbtests.NewIntegration(t, "test_db.db", nlog)
	t.Cleanup(test.Teardown)

	shutdown := make(chan os.Signal, 1)
	tests := PostTests{
		app: handlers.ApiMux(handlers.APIMuxConfig{
			Shutdown: shutdown,
			Log:      test.Log,
			Auth:     test.Auth,
			DB:       test.DB,
		}),
		userToken: test.Token("test@gmail.com", "gophers"),
	}

	// run tests
	t.Run("create201", tests.createPost201)
}

func (ut *PostTests) createPost201(t *testing.T) {}
