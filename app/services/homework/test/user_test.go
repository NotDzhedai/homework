package test

import (
	"homework/app/services/homework/handlers"
	"homework/business/data/dbtests"
	"log"
	"net/http"
	"os"
	"testing"
)

type UserTests struct {
	app       http.Handler
	userToken string
}

func TestUser(t *testing.T) {
	nlog := log.New(os.Stdout, " TEST USER ", log.Ltime|log.Lshortfile)
	test := dbtests.NewIntegration(t, "test_db.db", nlog)
	t.Cleanup(test.Teardown)

	shutdown := make(chan os.Signal, 1)
	tests := UserTests{
		app: handlers.ApiMux(handlers.APIMuxConfig{
			Shutdown: shutdown,
			Log:      test.Log,
			Auth:     test.Auth,
			DB:       test.DB,
		}),
		userToken: test.Token("test@gmail.com", "gophers"),
	}

	// run tests
	t.Run("create201", tests.createUser201)
}

func (ut *UserTests) createUser201(t *testing.T) {}
