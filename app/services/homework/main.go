package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"homework/app/services/homework/handlers"
	"homework/business/data/dbschema"
	"homework/business/sys/auth"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/ardanlabs/conf/v3"
	_ "github.com/mattn/go-sqlite3"
)

/*
TODO:

1 CRUD для постів
2 CRUD для коментів
3 Фронт
*/

// Не дуже люблю розбивати логіку в main файлі, тому він досить великий

// test end

func main() {
	// create logger
	nlog := log.New(os.Stdout, " HOMEWORK ", log.Ldate|log.Ltime|log.Lshortfile)

	if err := run(nlog); err != nil {
		nlog.Println(err)
		os.Exit(1)
	}
}

func run(nlog *log.Logger) error {
	// config
	cfg := struct {
		conf.Version
		Web struct {
			APIHost         string        `conf:"default:0.0.0.0:3000"`
			ReadTimeout     time.Duration `conf:"default:5s,noprint"`
			WriteTimeout    time.Duration `conf:"default:10s,noprint"`
			IdleTimeout     time.Duration `conf:"default:120s,noprint"`
			ShutdownTimeout time.Duration `conf:"default:20s,noprint"`
			CORSOrigin      string        `conf:"default:http://localhost:4200"`
		}
		Auth struct {
			JwtSecret string `conf:"default:secret"`
		}
	}{
		Version: conf.Version{
			Build: "build",
			Desc:  "fullstack hamework",
		},
	}

	help, err := conf.Parse("homework", &cfg)
	if err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			fmt.Println(help)
			return nil
		}
		return fmt.Errorf("parsing config: %w", err)
	}

	// starting app
	nlog.Println("starting app")
	defer nlog.Println("shutdown complete")

	out, err := conf.String(&cfg)
	if err != nil {
		return fmt.Errorf("generating config for output: %w", err)
	}
	nlog.Println("with config ", out)

	// database
	nlog.Println("init database support")

	// delete prev db
	nlog.Println("deleting prev db...")

	os.Remove("database.db")

	// create new file db
	nlog.Println("creating new db...")
	file, err := os.Create("database.db")
	if err != nil {
		log.Fatal(err)
	}
	file.Close()

	db, err := sql.Open("sqlite3", "./database.db")
	if err != nil {
		log.Fatal(err)
	}

	// ? migrations
	// migrate
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.Migrate(ctx, db); err != nil {
		log.Fatal("Migrate fail: ", err)
	}
	// seed
	ctx, cancel = context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.Seed(ctx, db); err != nil {
		log.Fatal("Seed fail: ", err)
	}

	defer func() {
		nlog.Println("stopping database support")
		db.Close()
	}()

	// auth

	nlog.Println("init auth support")

	auth, err := auth.New(cfg.Auth.JwtSecret)
	if err != nil {
		nlog.Fatal(err)
	}

	// start API
	nlog.Println("starting api")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	apiMux := handlers.ApiMux(handlers.APIMuxConfig{
		Shutdown: shutdown,
		Log:      nlog,
		DB:       db,
		Auth:     auth,
	}, handlers.WithCORS(cfg.Web.CORSOrigin))

	api := http.Server{
		Addr:         cfg.Web.APIHost,
		Handler:      apiMux,
		ReadTimeout:  cfg.Web.ReadTimeout,
		WriteTimeout: cfg.Web.WriteTimeout,
		IdleTimeout:  cfg.Web.IdleTimeout,
		ErrorLog:     log.New(os.Stdout, "SERVER_ERROR", log.Ldate|log.Ltime|log.Lshortfile),
	}

	serverErrors := make(chan error, 1)

	go func() {
		nlog.Println("start on host ", api.Addr)
		serverErrors <- api.ListenAndServe()
	}()

	// shutdown

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)
	case sig := <-shutdown:
		nlog.Println("shutdown started ", sig)
		defer nlog.Println("shutdown complete ", sig)

		// Give outstanding requests a deadline for completion.
		ctx, cancel := context.WithTimeout(context.Background(), cfg.Web.ShutdownTimeout)
		defer cancel()

		// Asking listener to shutdown and shed load.
		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
