module homework

go 1.19

require (
	github.com/ardanlabs/conf/v3 v3.1.3
	github.com/dimfeld/httptreemux/v5 v5.5.0
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.11.1
	github.com/golang-jwt/jwt/v4 v4.4.2
	github.com/google/go-cmp v0.5.6
	github.com/google/uuid v1.3.0
	github.com/mattn/go-sqlite3 v1.14.16
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
)

require (
	github.com/leodido/go-urn v1.2.1 // indirect
	golang.org/x/sys v0.0.0-20220317061510-51cd9980dadf // indirect
	golang.org/x/text v0.3.7 // indirect
)
