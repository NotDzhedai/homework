import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Post } from '../models/post';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
})
export class UserPostsComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  loading = false;
  posts: Post[] = [];

  constructor(private postsService: PostsService) {}

  onDelete(id: string) {
    this.postsService
      .deletePost(id)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => {
          console.log('Post deleted');
          this.getPosts();
        },
        error: (err) => console.log(err),
      });
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  ngOnInit(): void {
    this.loading = true;
    this.getPosts();
  }

  getPosts() {
    this.postsService
      .getUserPosts()
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: (data) => {
          this.posts = data;
          this.loading = false;
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
