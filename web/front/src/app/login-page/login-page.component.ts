import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})
export class LoginPageComponent implements OnInit {
  notifier = new Subject<void>();

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  onSubmit() {
    if (this.form.invalid) return;

    this.form.disable();
    this.authService
      .login(
        this.form.controls['email'].value,
        this.form.controls['password'].value
      )
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => this.router.navigate(['/all']),
        error: (err) => {
          console.log(err);
          this.form.reset();
          this.form.enable();
        },
      });
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: this.fb.control(null, [Validators.required, Validators.email]),
      password: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }
}
