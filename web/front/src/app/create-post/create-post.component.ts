import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css'],
})
export class CreatePostComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  form: FormGroup;

  constructor(private fb: FormBuilder, private postsService: PostsService) {}

  onSubmit() {
    if (this.form.invalid) return;

    this.form.disable();
    this.postsService
      .createPost(this.form.value)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => console.log('Post created'),
        error: (err) => console.log(err),
        complete: () => {
          this.form.reset();
          this.form.enable();
        },
      });
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      title: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
      body: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }
}
