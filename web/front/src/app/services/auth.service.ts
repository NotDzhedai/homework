import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, tap } from 'rxjs';
import { NewUser, User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  addr: string = 'http://localhost:3000/v1';

  private token: string | null = null;

  register(nu: NewUser): Observable<User> {
    return this.http.post<User>(this.addr + '/user', nu);
  }

  login(emai: string, password: string): Observable<{ token: string }> {
    return this.http
      .get<{ token: string }>(this.addr + '/login', {
        headers: {
          Authorization: 'Basic ' + btoa(`${emai}:${password}`),
          skip: 'true',
        },
      })
      .pipe(
        tap(({ token }) => {
          localStorage.setItem('token', token);
          this.setToken(token);
        })
      );
  }

  setToken(token: string | null) {
    this.token = token;
  }

  getToken(): string | null {
    return this.token;
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  logOut() {
    this.setToken(null);
    localStorage.clear();
    this.router.navigate(['/login'], {
      queryParams: {
        logout: true,
      },
    });
  }
}
