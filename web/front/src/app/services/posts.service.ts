import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NewPost, Post } from '../models/post';

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  addr: string = 'http://localhost:3000/v1';

  constructor(private http: HttpClient) {}

  getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.addr + '/posts');
  }

  getUserPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.addr + '/posts_user');
  }

  createPost(np: NewPost): Observable<Post> {
    return this.http.post<Post>(this.addr + '/post', np);
  }

  getPost(id: string): Observable<Post> {
    return this.http.get<Post>(this.addr + `/post/${id}`);
  }

  updatePost(up: NewPost, id: string): Observable<void> {
    return this.http.put<void>(this.addr + `/post/${id}`, up);
  }

  deletePost(id: string): Observable<void> {
    return this.http.delete<void>(this.addr + `/post/${id}`);
  }
}
