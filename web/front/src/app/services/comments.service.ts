import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Comment, NewComment } from '../models/comment';

@Injectable({
  providedIn: 'root',
})
export class CommentsService {
  addr: string = 'http://localhost:3000/v1';

  constructor(private http: HttpClient) {}

  getComments(postId: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.addr + `/comments/${postId}`);
  }

  addComment(nc: NewComment): Observable<Comment> {
    return this.http.post<Comment>(this.addr + '/comment', nc);
  }
}
