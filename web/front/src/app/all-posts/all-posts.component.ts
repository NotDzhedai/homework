import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { Post } from '../models/post';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
})
export class AllPostsComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  loading = false;
  posts: Post[] = [];

  constructor(private postsService: PostsService) {}

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  ngOnInit(): void {
    this.loading = true;
    this.postsService
      .getPosts()
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: (data) => {
          this.posts = data;
          this.loading = false;
        },
        error: (err) => {
          console.log(err);
        },
      });
  }
}
