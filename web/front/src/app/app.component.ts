import { Component } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {
  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    const potentialToken = localStorage.getItem('token');
    if (potentialToken !== null) {
      this.authService.setToken(potentialToken);
    }
  }
}
