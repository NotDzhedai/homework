import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Post } from '../models/post';
import { Comment } from '../models/comment';

import { CommentsService } from '../services/comments.service';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-view-post',
  templateUrl: './view-post.component.html',
  styleUrls: ['./view-post.component.css'],
})
export class ViewPostComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  postID: string;
  post: Post;
  comments: Comment[] = [];

  loadingPost = false;
  loadingComments = false;

  constructor(
    private route: ActivatedRoute,
    private postsService: PostsService,
    private commentsService: CommentsService,
    private fb: FormBuilder
  ) {}

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  ngOnInit(): void {
    this.loadingPost = true;
    this.loadingComments = true;

    this.route.params.subscribe((params) => {
      this.postID = params['id'];
    });

    this.getPost();
    this.getComments();
  }

  addComment(newCommet: string) {
    this.commentsService
      .addComment({ comment: newCommet, post_id: this.postID })
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => {
          this.getComments();
        },
        error: (err) => console.log(err),
      });
  }

  getPost() {
    this.postsService
      .getPost(this.postID)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: (data) => {
          this.post = data;
          this.loadingPost = false;
        },
        error: (err) => console.log(err),
      });
  }

  getComments() {
    this.commentsService
      .getComments(this.postID)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: (data) => {
          this.comments = data;
          this.loadingComments = false;
        },
        error: (err) => console.log(err),
      });
  }
}
