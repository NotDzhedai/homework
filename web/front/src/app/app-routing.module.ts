import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllPostsComponent } from './all-posts/all-posts.component';
import { AuthGuard } from './auth.guard';
import { BaseLayoutComponent } from './base-layout/base-layout.component';
import { CreatePostComponent } from './create-post/create-post.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { UpdatePostComponent } from './update-post/update-post.component';
import { UserPostsComponent } from './user-posts/user-posts.component';
import { ViewPostComponent } from './view-post/view-post.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent },
  {
    path: '',
    canActivate: [AuthGuard],
    component: BaseLayoutComponent,
    children: [
      { path: 'all', component: AllPostsComponent },
      { path: 'user-posts', component: UserPostsComponent },
      { path: 'create', component: CreatePostComponent },
      { path: 'edit/:id', component: UpdatePostComponent },
      { path: 'post/:id', component: ViewPostComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
