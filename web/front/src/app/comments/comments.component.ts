import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from '../models/comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
})
export class CommentsComponent implements OnInit {
  @Input() loading: boolean;
  @Input() comments: Comment[];
  @Output() onAddComment = new EventEmitter<string>();

  newComment: string;

  constructor() {}

  AddComment() {
    this.onAddComment.emit(this.newComment);
    this.newComment = '';
  }

  ngOnInit(): void {}
}
