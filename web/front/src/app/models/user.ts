export interface User {
  id: string;
  name: string;
  email: string;
  date_created: Date;
}

export interface NewUser {
  name: string;
  email: string;
  password: string;
}
