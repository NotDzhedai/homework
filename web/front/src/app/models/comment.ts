export interface Comment {
  id: string;
  comment: string;
  user_id: string;
  post_id: string;
  date_created: Date;
}

export interface NewComment {
  comment: string;
  post_id: string;
}
