export interface Post {
  id: string;
  title: string;
  body: string;
  user_id: string;
  date_created: Date;
}

export interface NewPost {
  title: string;
  body: string;
}

export interface UpdatePost {
  title: string;
  body: string;
}
