import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { Post } from '../models/post';
import { Comment } from '../models/comment';
import { CommentsService } from '../services/comments.service';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.css'],
})
export class UpdatePostComponent implements OnInit, OnDestroy {
  notifier = new Subject<void>();

  loadingPost = false;
  loadingComments = false;

  postID: string;
  post: Post;
  comments: Comment[] = [];
  form: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private postsService: PostsService,
    private commentsService: CommentsService,
    private fb: FormBuilder
  ) {}

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    this.form.disable();
    this.postsService
      .updatePost(this.form.value, this.post.id)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => console.log('Post updated'),
        error: (err) => console.log(err),
        complete: () => {
          this.form.reset();
          this.form.enable();
        },
      });

    this.getPost();
  }

  getPost() {
    this.postsService
      .getPost(this.postID)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: (data) => {
          this.post = data;
          this.form.controls['title'].setValue(this.post.title);
          this.form.controls['body'].setValue(this.post.body);
          this.loadingPost = false;
        },
        error: (err) => console.log(err),
      });
  }

  getComments() {
    this.commentsService
      .getComments(this.postID)
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: (data) => {
          this.comments = data;
          this.loadingComments = false;
        },
        error: (err) => console.log(err),
      });
  }

  addComment(newCommet: string) {
    this.commentsService
      .addComment({ comment: newCommet, post_id: this.postID })
      .pipe(takeUntil(this.notifier))
      .subscribe({
        next: () => {
          this.getComments();
        },
        error: (err) => console.log(err),
      });
  }

  ngOnDestroy(): void {
    this.notifier.next();
    this.notifier.complete();
  }

  ngOnInit(): void {
    this.loadingPost = true;
    this.loadingComments = true;

    this.form = this.fb.group({
      title: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
      body: this.fb.control(null, [
        Validators.required,
        Validators.minLength(4),
      ]),
    });

    this.route.params.subscribe((params) => {
      this.postID = params['id'];
    });

    this.getPost();
    this.getComments();
  }
}
